import { Controller, Get, Param } from '@nestjs/common';
import { ManagerBookingsService } from './manager-bookings.service';

@Controller('manager/bookings')
export class ManagerBookingsController {
    
    constructor(
        private readonly managerBookingsService : ManagerBookingsService
    ){}

    @Get('/completed')
    async completed_transactions(){}

    @Get('gross')
    async gross(){}

    @Get('/charger/:chargerId')
    async charger_transactions(
        @Param() params
    ){}

    @Get('/last/:limit')
    async last_bookings_limit(
        @Param() params
    ){}

}
