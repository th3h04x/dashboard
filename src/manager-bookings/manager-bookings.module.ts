import { Module } from '@nestjs/common';
import { ManagerBookingsService } from './manager-bookings.service';
import { ManagerBookingsController } from './manager-bookings.controller';

@Module({
  providers: [ManagerBookingsService],
  controllers: [ManagerBookingsController]
})
export class ManagerBookingsModule {}
