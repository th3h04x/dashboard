import { Controller , Post} from '@nestjs/common';
import { FeedbacksService } from './feedbacks.service';

@Controller('feedbacks')
export class FeedbacksController {
    constructor(
        private readonly feedbacksService : FeedbacksService
      ){}
    
      @Post('/rate')
      async giveRatings(){}

      @Post('/review')
      async giveReview(){}

}
