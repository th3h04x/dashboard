import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ManagerModule } from './manager/manager.module';
import { ChargerModule } from './charger/charger.module';
import { FinanceModule } from './finance/finance.module';
import { ManagerBookingsModule } from './manager-bookings/manager-bookings.module';
import { FeedbacksModule } from './feedbacks/feedbacks.module';

@Module({
  imports: [ManagerModule, ChargerModule, FinanceModule, ManagerBookingsModule, FeedbacksModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
