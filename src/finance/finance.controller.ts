import { Controller, Get, Post } from '@nestjs/common';
import { FinanceService } from './finance.service';

@Controller('finance')
export class FinanceController {
    constructor(
        private readonly financeService : FinanceService
    ){}

    @Post('/requestMoney')
    async requestMoney(){

    }

    @Get('/manager')
    async getManagerFinance(){

    }
}
