import { Controller, Get, Patch, Post } from '@nestjs/common';
import { ManagerService } from './manager.service';

@Controller('manager')
export class ManagerController {
    constructor(
        private readonly managerService : ManagerService
    ){}

    @Post('/manager')
    async login(){

    }

    @Get('/me')
    async myDetail(){

    }

    @Post('/changePassword')
    async changePassword(){

    }

    @Patch('/')
    async editManager(){

    }

    @Post('/verifyPassword')
    async verifyPassword(){

    }

    @Get('/currentBalance')
    async currentBalance(){

    }

    @Patch('/editBankDetails')
    async editBankDetails(){

    }

    @Post('/forgotPassword')
    async forgotPassword(){

    }

    @Post('/resetPassword')
    async resetPassword(){

    }

    @Post('/validateForgotPasswordOTP')
    async validateForgotPasswordOTP(){

    }


}
