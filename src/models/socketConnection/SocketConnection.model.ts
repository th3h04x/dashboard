import { Prop , Schema } from '@nestjs/mongoose';
import moment from 'moment';
import {Date, Document} from 'mongoose'
import * as mongoose from 'mongoose'
import { User } from '../user/User.models';

@Schema()
export class SocketConnection extends Document{
    @Prop({
        type: String,
        required: true,
    })
    _id : string

    @Prop({
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true
    })
    userId : User

    @Prop({
        type: String,
        ref: "Charger",
        required: true
    })
    chargerId : string
}