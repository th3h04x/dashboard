import * as mongoose from 'mongoose'
import * as moment from 'moment'
import {Date, Document} from 'mongoose'
import { Prop , Schema} from '@nestjs/mongoose'

@Schema()
export class ChargerBooking extends Document {

    @Prop({
        type:String,
        required:[true , "name is mandatory"]
    })
    name : string

    @Prop({
        type:String,
        required:[true , "email is mandatory"]
    })
    email : string

    @Prop({
        type:String,
        required:[true , "contact is mandatory"]
    })
    contact : string

    @Prop( {
        type: String,
        required:[true , "address must be given"]
    })
    location : string

    @Prop({
        type: Date,
        default: moment().toDate()
    })
    createdAt : any

    @Prop({
        type:String,
        default:"Not Processed"
    })
    status : string

}
