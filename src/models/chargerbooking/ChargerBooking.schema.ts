import { SchemaFactory } from "@nestjs/mongoose";
import { ChargerBooking } from "./ChargerBooking.model";
import * as moment from 'moment'

export const ChargerBookingSchema = SchemaFactory.createForClass(ChargerBooking)

ChargerBookingSchema.pre<ChargerBooking>('save',function(next : Function){
    if(this.isNew){
        this.createdAt = moment().toDate()
    }
    next()
})

