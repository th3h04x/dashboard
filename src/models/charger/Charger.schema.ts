import {SchemaFactory} from '@nestjs/mongoose'
import moment from 'moment'
import { Charger } from './Charger.models'

export const ChargerSchema = SchemaFactory.createForClass(Charger)

ChargerSchema.index({location : '2dsphere'})
