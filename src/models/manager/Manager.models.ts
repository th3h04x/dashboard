import { Prop , Schema } from '@nestjs/mongoose';
import moment from 'moment';
import {Date, Document} from 'mongoose'

export class bankDetails extends Document{
    @Prop({
        type : String
    })
    accountNumber : string

    @Prop({
        type : String
    })
    IFSC : string

    @Prop({
        type : String
    })
    accountHolderName : string

    @Prop({
        type : String
    })
    bankName : string

    @Prop({
        type : String
    })
    accountType : string

    @Prop({
        type : String
    })
    bankAddress : string
}

@Schema({
    timestamps: true
})
export class Manager extends Document{
    @Prop({
            unique:[true , "username should be unique"],
            type : String
        })
    username :string

    @Prop({
        required:true,
        select:false,
        type : String
    })
    password : string

    @Prop({
        type : String
    })
    name : string

    @Prop({
        enum : ["admin" , "manager"] ,
        default : "manager" ,
        immutable : true,
        type : String
    })
    role : string

    @Prop({
        default: moment().toDate(),
        type: Date
    })
    createdAt : any

    @Prop({
        select : false,
        type: Date
    })
    passwordChangedAt : Date

    @Prop({
        default : 5 ,
        max: [15, "Manager share can't be greater than 15"],
        type: Number
    })
    sharePercentage : number

    @Prop({
        default : 15 ,
        type: Number
    })
    serviceChargePercentage : number

    @Prop({
        type : String
    })
    number : string

    @Prop({
        type : String
    })
    location : string

    @Prop({
        select : false,
        type: Number
    })
    forgotOTP : number

    @Prop({
        select : false,
        type : Date
    })
    OTPExpiresAt : Date

    @Prop({
        type : bankDetails
    })
    bankDetails : bankDetails

}

