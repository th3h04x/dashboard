import { SchemaFactory } from "@nestjs/mongoose";
import { User } from "./User.models";


export const UserSchema = SchemaFactory.createForClass(User)
