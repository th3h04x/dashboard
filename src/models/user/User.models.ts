import { Prop , Schema } from '@nestjs/mongoose';
import moment from 'moment';
import {Date, Document} from 'mongoose'

@Schema({
    timestamps: true
})
export class User extends Document {
    @Prop({
        type: String,
        unique: [true, "This number already exists!"]
    })
    number : string

    @Prop({
        type: String,
        unique: [true, "This email already exists!"]
    })
    email : string

    @Prop({
        type : String
    })
    name : string

    @Prop({
        type : String
    })
    city : string

    @Prop({
        type: String
    })
    username : string

    @Prop({
        type: Number
    })
    mobileVerifyOTP : number

    @Prop( {
        type: Number,
        max: [5, "Value can't exceed 5"],
        min: [0, "Value can't be less than 0"]
    })
    rateUs : number

    @Prop({
        type: Number
    })
    firebaseUid : number
}