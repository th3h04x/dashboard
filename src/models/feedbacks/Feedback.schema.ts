import { SchemaFactory } from "@nestjs/mongoose";
import { Feedbacks } from "./Feedback.model";
import * as moment from 'moment'

export const FeedBackSchema = SchemaFactory.createForClass(Feedbacks)

FeedBackSchema.pre<Feedbacks>('save',function(next : Function){
    if(this.isNew){
        this.createdAt = moment().toDate()
    }
    next()
})