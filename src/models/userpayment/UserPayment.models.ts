import { Prop , Schema } from '@nestjs/mongoose';
import { User } from 'src/models/user/User.models';
import moment from 'moment';
import {Date, Document} from 'mongoose'
import * as mongoose from 'mongoose'
import { Manager } from 'src/models/manager/Manager.models';

@Schema()
export class UserPayment extends Document {
    @Prop({
       type : Number ,
       required : [true , "Energy is rquired"],
       set: val => Math.round(val*100)/100 
    })
    energyConsumend : number

    @Prop({
        type: Number,
        required: [true, "amount is required"],
        set: val => Math.round(val*100)/100
    })
    amount : number

    @Prop({
        type: Number
    })
    connectedTime : number

    @Prop({
        type: Number,
        set: val => Math.round(val*100)/100
    })
    managerAmount : number

    @Prop({
        type: Date,
        default: moment().toDate()
    })
    createdAt : any

    @Prop({
        type: String,
        ref: "chargers",
        required: [true, "chargerId is required"]
    })
    chargerId : string

    @Prop({
        type : mongoose.Schema.Types.ObjectId ,
        ref : 'User',
        required: [true, "userId is required"]
    })
    userId : User

    @Prop({
        type : mongoose.Schema.Types.ObjectId ,
        ref : 'Manager'
    })
    managerId : Manager

    @Prop({
        type: Boolean,
        default: false
    })
    payStatus : boolean

    @Prop({
        type : String
    })
    orderId : string

    @Prop({
        type : String
    })
    paymentId : string

    @Prop({
        type : String
    })
    paymentMadeAt : Date

}

