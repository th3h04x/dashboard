import * as mongoose from 'mongoose'
import * as moment from 'moment'
import {Date, Document} from 'mongoose'
import {Prop, Schema} from '@nestjs/mongoose'
import { User } from '../user/User.models'
import { Manager } from '../manager/Manager.models'

@Schema({
    timestamps: true
})
export class Rating extends Document{
    @Prop({
        type: Number,
        max: [5, "Value can't exceed 5"],
        min: [0, "Value can't be less than 0"]
    })
    ratings : number

    @Prop({
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        default:null
    })
    userId : User
    
    @Prop({
        type: mongoose.Schema.Types.ObjectId,
        ref: "Manager",
        default:null
    })
    managerId : Manager

    @Prop({
        type: Date,
        default: moment().toDate()
    })
    createdAt : any

    @Prop({
        type: String,
        required: [true , "source is required"]
    })
    source : string

    @Prop({
        type: String
    })
    message : string
}