import { SchemaFactory } from "@nestjs/mongoose";
import { Finance } from "./Finance.model";
import * as moment from 'moment'

export const FinanceSchema = SchemaFactory.createForClass(Finance)

FinanceSchema.pre<Finance>('save',function(next : Function){
    if(this.isNew){
        this.createdAt = moment().toDate()
    }
    next()
})