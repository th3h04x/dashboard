import { Controller, Get, Post } from '@nestjs/common';
import { ChargerService } from './charger.service';

@Controller('charger')
export class ChargerController {
    constructor(
        private readonly chargerService : ChargerService
    ){}

    @Get('/manager')
    async manager_chargers(){}

    @Get('/manager/gross/:chargerId')
    async manager_charger_gross(){}

    @Get('/manager/activeCount')
    async manager_charger_activeCount(){}

    @Post('/setRegularPrice')
    async setRegularPrice(){}

    @Post('/setSpecialPrice')
    async setSpecialPrice(){}

}
